using Microsoft.EntityFrameworkCore;

namespace Omniarchive.DbModel.Versions; 

public class McDb : DbContext {
    public DbSet<McDbInfo> McDbInfos { get; set; }
    public McDb(DbContextOptions? optionsBuilder = null)
    {
        OnConfiguring(new DbContextOptionsBuilder(optionsBuilder ?? new DbContextOptionsBuilder().Options));
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql($"User ID=omnibot; Password=omnibot; Host=localhost; Database=omnidex");
    }
}
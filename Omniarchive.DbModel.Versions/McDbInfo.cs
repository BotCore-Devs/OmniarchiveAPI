using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Omniarchive.DbModel.Versions;

public class McDbInfo
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [TableIgnore]
    public int McVersionId { get; set; }

    [TableIgnore]
    public string? GamePlatform { get; set; } = "";

    [TableIgnore]
    public string? Environment { get; set; } = "";

    [JsonIgnore]
    [TableIgnore]
    public string? EntryType { get; set; }

    [FriendlyName("")]
    public string? Type { get; set; }

    [FriendlyName("ID")]
    public string? Id { get; set; }

    public string? Version { get; set; }

    [FriendlyName("PVN")]
    public string? Pvn { get; set; }

    [FriendlyName("Release date")]
    public string? ReleaseDate { get; set; }

    [FriendlyName("Proof")]
    public string? ProofOfExistence { get; set; }

    [JsonIgnore]
    [FriendlyName("Notes")]
    public string? Notes { get; set; }

    [FriendlyName("MD5")]
    public string? Md5 { get; set; }

    [FriendlyName("SHA256")]
    public string? Sha256 { get; set; }

    [TableIgnore]
    [RowColor]
    public string? Color { get; set; }

    [TableIgnore]
    public string? Format { get; set; }

    [TableIgnore]
    public string? Os { get; set; }

    [TableIgnore]
    public string? FoundStatus => Color switch
    {
        "93C47D" => "Found",
        "6FA8DC" => "Edited",
        "6D9EEB" => "Edited",
        "E06666" => "Unfound",
        "FFD966" => "No proof",
        _ => "Unknown?"
    };

    //index mapping
    [NotMapped]
    public string? Released
    {
        set => ReleaseDate = value;
    }

    [NotMapped]
    public string? Platform
    {
        set => Format = value;
    }
}
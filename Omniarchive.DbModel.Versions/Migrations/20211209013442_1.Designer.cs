﻿// <auto-generated />
using BotCore.BotExtensions.Omnibot.DBTest.McDbModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using Omniarchive.DbModel.Versions;

#nullable disable

namespace BotCore.BotExtensions.Omnibot.DBTest.McDbModel.Migrations
{
    [DbContext(typeof(McDb))]
    [Migration("20211209013442_1")]
    partial class _1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("BotCore.BotExtensions.Omnibot.DBTest.McDbModel.McDbInfo", b =>
                {
                    b.Property<int>("McVersionId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("McVersionId"));

                    b.Property<string>("Color")
                        .HasColumnType("text");

                    b.Property<string>("EntryType")
                        .HasColumnType("text");

                    b.Property<string>("Environment")
                        .HasColumnType("text");

                    b.Property<string>("Format")
                        .HasColumnType("text");

                    b.Property<string>("GamePlatform")
                        .HasColumnType("text");

                    b.Property<string>("Id")
                        .HasColumnType("text");

                    b.Property<string>("Md5")
                        .HasColumnType("text");

                    b.Property<string>("Notes")
                        .HasColumnType("text");

                    b.Property<string>("Os")
                        .HasColumnType("text");

                    b.Property<string>("ProofOfExistence")
                        .HasColumnType("text");

                    b.Property<string>("Pvn")
                        .HasColumnType("text");

                    b.Property<string>("ReleaseDate")
                        .HasColumnType("text");

                    b.Property<string>("Sha256")
                        .HasColumnType("text");

                    b.Property<string>("Type")
                        .HasColumnType("text");

                    b.Property<string>("Version")
                        .HasColumnType("text");

                    b.HasKey("McVersionId");

                    b.ToTable("McDbInfos");
                });
#pragma warning restore 612, 618
        }
    }
}

using ArcaneLibs;

namespace OmniarchiveAPI;

public class Config : SaveableObject<Config>
{
    public string SentryEnvironment { get; } = "";
}
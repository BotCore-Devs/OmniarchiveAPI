﻿using System.Diagnostics;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Omniarchive.DbModel.Versions;
using OmniarchiveAPI.Helpers;
using OmniarchiveIndexParser;
using Index = OmniarchiveIndexParser.Index;

namespace OmniarchiveAPI.Controllers {
    [Controller]
    [Route("/")]
    public class VersionDataController : ControllerBase {
        [HttpGet("indtest")]
        public ContentResult GetPvr(bool includeFound = true, string table = "") {
            return new ContentResult() { Content = DataUtils.ListToHtmlTable(new McDb().McDbInfos.ToList()), ContentType = "text/html" };
        }
    }
}
using Microsoft.AspNetCore.Mvc;
using OmniarchiveAPI.Helpers;

namespace OmniarchiveAPI.Controllers.Frontend;

[Controller]
[Route("/")]
public class FrontendController : Controller
{
    private readonly Db _db;

    public FrontendController(Db db)
    {
        _db = db;
    }

    [HttpGet]
    public object Home()
    {
        Console.WriteLine(Request.Host);
        var bot = Resolvers.GetBotByHost(_db, Request.Host.Host);
        return Resolvers.ReturnFileWithVars("Resources/Pages/index.html", _db, bot);
    }
}
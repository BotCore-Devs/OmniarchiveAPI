﻿using System.Diagnostics;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OmniarchiveIndexParser;
using Index = OmniarchiveIndexParser.Index;

namespace OmniarchiveAPI.Controllers {
    [Controller]
    [Route("/")]
    public class HomeControllers : ControllerBase
    {

        [HttpGet("index/pvr/json")]
        public ContentResult GetPvrJson(bool includeFound = true, int page = 0, int count = int.MaxValue,
            string table = "")
        {
            return new ContentResult()
            {
                Content = @$"Welcome to the Omniarchive API!

Endpoints:
- /index/json
Returns java clients as json
- /index/table
Returns java clients as table",
                ContentType = "text/html"
            };
        }
    }
}
using Microsoft.AspNetCore.Mvc;
using OmniarchiveAPI.Helpers;

namespace OmniarchiveAPI.Controllers.Frontend;

[Controller]
[Route("/leaderboards")]
public class LeaderboardController : Controller
{
    private readonly Db _db;

    public LeaderboardController(Db db)
    {
        _db = db;
    }

    [HttpGet]
    public object Home()
    {
        Console.WriteLine(Request.Host);
        var bot = Resolvers.GetBotByHost(_db, Request.Host.Host);
        return Resolvers.ReturnFileWithVars("Resources/Pages/Leaderboards/index.html", _db, bot);
    }

    [HttpGet("{id}")]
    public object Home(ulong id)
    {
        Console.WriteLine(Request.Host);
        var bot = Resolvers.GetBotByHost(_db, Request.Host.Host);
        return Resolvers.ReturnFileWithVars("Resources/Pages/Leaderboards/ServerId/index.html", _db, bot);
    }

    [HttpGet("{serverid}/{userid}")]
    public object Home(ulong serverid, ulong userid)
    {
        Console.WriteLine(Request.Host);
        var bot = Resolvers.GetBotByHost(_db, Request.Host.Host);
        return Resolvers.ReturnFileWithVars("Resources/Pages/Leaderboards/ServerId/index.html", _db, bot);
    }
}
using Microsoft.AspNetCore.Mvc;
using OmniarchiveAPI.Helpers;

namespace OmniarchiveAPI.Controllers.Frontend;

[Controller]
[Route("/")]
public class StaticController : Controller
{
    private readonly Db _db;

    public StaticController(Db db)
    {
        _db = db;
    }

    [HttpGet("/static/{*res:required}")]
    public object Resource(string res)
    {
        return Resolvers.ReturnFile("./Resources/Static/" + res);
    }

    [HttpGet("/robots.txt")]
    public object Robots()
    {
        return Resolvers.ReturnFile("./Resources/robots.txt");
    }

    [HttpGet("/favicon.ico")]
    public object Favicon()
    {
        return Resolvers.ReturnFile("./Resources/RunData/" + Resolvers.GetBotByHost(_db, Request.Host.Host).Name +
                                    ".png");
    }
}
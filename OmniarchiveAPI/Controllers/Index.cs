﻿using System.Diagnostics;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OmniarchiveIndexParser;
using Index = OmniarchiveIndexParser.Index;

namespace OmniarchiveAPI.Controllers
{
    //legacy code

    [Controller]
    [Route("/aaa")]
    public class IndexController : ControllerBase
    {
        [HttpGet("index/pvr/json")]
        public ContentResult GetPvrJson(bool includeFound = true, int page = 0, int count = int.MaxValue,
            string table = "")
        {
            return new ContentResult()
            {
                Content = GetIndexJson(Index.Instance.VersionList, includeFound, page, count, table),
                ContentType = "text/html"
            };
        }

        [HttpGet("index/pvr")]
        public ContentResult GetPvr(bool includeFound = true, string table = "")
        {
            return new ContentResult()
                {Content = GetIndex(Index.Instance.VersionList, includeFound, table), ContentType = "text/html"};
        }

        [HttpGet("index/columns")]
        public ContentResult GetColumns()
        {
            return new ContentResult()
            {
                Content = @"<tr style='background-color: #000; color: #999;'>
    <td>Status</td>
    <td>Type</td>
    <td>ID</td>
    <td>Version</td>
    <td>PVN</td>
    <td>Release Date</td>
    <td>Proof of Existence</td>
    <td>Format</td>
    <td>Notes</td>
    <td>MD5</td>
    <td>SHA-256</td>
</tr>",
                ContentType = "text/html"
            };
        }

        [HttpGet("test")]
        public ContentResult GetPg()
        {
            return new ContentResult
            {
                Content =
                    $"<table>\n{GetColumns().Content}\n{GetIndex(Index.Instance.VersionList, true, "javaclients")}</table>",
                ContentType = "text/html"
            };
        }

        [HttpGet("index")]
        public ContentResult Get()
        {
            return new ContentResult
            {
                Content = new WebClient().DownloadString(
                    "https://omniarchive.thearcanebrony.net/static/templates/indexview/basepage.html"),
                ContentType = "text/html"
            };
        }

        [HttpGet("indexold")]
        public ContentResult GetOld()
        {
            return new ContentResult
            {
                Content = $@"<script src=""https://code.jquery.com/jquery-3.5.1.min.js""></script>
<style>
  center{{ color: white; }}
  .avatar {{ border-radius:50%; height:10%; }}
  h1, h2, h3, h4, h5, h6, p {{font-family: 'Segoe UI';}}
  #footer {{
    position: fixed;
    bottom: 0;
    vertical-align: bottom;
    width: auto;
    height: 24px;
    border: none;
  }}
td {{
    white-space: nowrap;
    min-width: 0px;
}}
tr:hover, tr > *:hover, tr > * > *:hover {{
font-size: normal !important;
                }}
table {{min-width: 100%;}}


/* Style the tab */
.tab {{
  overflow: hidden;
  background-color: #262626;
}}

/* Style the buttons that are used to open the tab content */
.tab button {{
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  color: #ccc;
}}

/* Change background color of buttons on hover */
.tab button:hover {{
  background-color: #ddd;
  color: #420;
}}

/* Create an active/current tablink class */
.tab button.active {{
  background-color: #ccc;
  color: #420;
}}

/* Style the tab content */
.tabcontent {{
  display: none;
}}
.loader {{
  color: #ffffff;
  font-size: 90px;
  text-indent: -9999em;
  overflow: hidden;
  width: 1em;
  height: 1em;
  border-radius: 50%;
  margin: 72px auto;
  position: relative;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation: load6 1.7s infinite ease, round 1.7s infinite ease;
  animation: load6 1.7s infinite ease, round 1.7s infinite ease;
}}
@-webkit-keyframes load6 {{
  0% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
  5%,
  95% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
  10%,
  59% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.087em -0.825em 0 -0.42em, -0.173em -0.812em 0 -0.44em, -0.256em -0.789em 0 -0.46em, -0.297em -0.775em 0 -0.477em;
  }}
  20% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.338em -0.758em 0 -0.42em, -0.555em -0.617em 0 -0.44em, -0.671em -0.488em 0 -0.46em, -0.749em -0.34em 0 -0.477em;
  }}
  38% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.377em -0.74em 0 -0.42em, -0.645em -0.522em 0 -0.44em, -0.775em -0.297em 0 -0.46em, -0.82em -0.09em 0 -0.477em;
  }}
  100% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
}}
@keyframes load6 {{
  0% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
  5%,
  95% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
  10%,
  59% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.087em -0.825em 0 -0.42em, -0.173em -0.812em 0 -0.44em, -0.256em -0.789em 0 -0.46em, -0.297em -0.775em 0 -0.477em;
  }}
  20% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.338em -0.758em 0 -0.42em, -0.555em -0.617em 0 -0.44em, -0.671em -0.488em 0 -0.46em, -0.749em -0.34em 0 -0.477em;
  }}
  38% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.377em -0.74em 0 -0.42em, -0.645em -0.522em 0 -0.44em, -0.775em -0.297em 0 -0.46em, -0.82em -0.09em 0 -0.477em;
  }}
  100% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
}}
@-webkit-keyframes round {{
  0% {{
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }}
  100% {{
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }}
}}
@keyframes round {{
  0% {{
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }}
  100% {{
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }}
}}

</style>
<script>
function openTab(evt, cityName) {{
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class='tabcontent' and hide them
  tabcontent = document.getElementsByClassName('tabcontent');
  for (i = 0; i < tabcontent.length; i++) {{
    tabcontent[i].style.display = 'none';
  }}

  // Get all elements with class='tablinks' and remove the class 'active'
  tablinks = document.getElementsByClassName('tablinks');
  for (i = 0; i < tablinks.length; i++) {{
    tablinks[i].className = tablinks[i].className.replace(' active', '');
  }}

  // Show the current tab, and add an 'active' class to the button that opened the tab
  document.getElementById(cityName).style.display = 'block';
  evt.currentTarget.className += ' active';
}}
function initTables(tab){{
  $(""#""+tab).prepend(""<center>Java Clients</center><table id='javaclients'></table>"")
  $(""#""+tab).prepend(""<center>Java Servers</center><table id='javaservers'></table>"")
}}
window.onload = function(e) {{
  openTab(e, 'RewriteUnfound');
  //var tabs = $("".tabcontent"");
  //tabs.each(a=>{{initTables(tabs[a].id)}})
  //setTimeout(()=>{{$('#Rewrite').load('index/pvr')}}, 1500);
  //setTimeout(()=>{{$('#RewriteUnfound').load('index/pvr?includeFound=false&limit=45')}}, 250);
  //setTimeout(()=>{{$('#RewriteUnfound').load('index/pvr?includeFound=false')}}, 1000);
  //setTimeout(()=>{{$('#Original').load('index/pv')}}, 1000);
}};
function initTables(tab){{
    $('#'+tab).prepend(`<center>Java Servers</center><table id='javaservers'></table>`)
    $('#'+tab).prepend(`<center>Java Clients</center><table id='javaclients'></table>`)
    $('table').load('index/columns');
}}
//initTables('RewriteUnfound')
function loadTable(target, endpoint, page, count, table, includeFound){{
    $.getJSON(`index/${{endpoint}}?includeFound=${{includeFound}}&page=${{page}}&count=${{count}}&table=${{table}}`, function(data){{
    console.log(data)
    console.log(page)
    count = data.Count;
    var html = '';
    data.Versions.forEach(ver => html += toRow(ver));
    target.append(html);
    if(data.Versions.length > 0) setTimeout(loadTable(target,endpoint, ++page, count, table, includeFound), 2000);
    }})
}}
//http://localhost:81/index/columns
function init(){{
    var tabs = $('.tabcontent');
    var i = tabs.count;
    tabs.each(a=>{{
        initTables(tabs[a].id);
        var tab = $(tabs[a]);
        var tables = tab.children('table');
        setTimeout(()=>tables.each(b=>{{
            table = $(tables[b]);
            console.log(table.parent()[0].id);
            console.log(tables[b].id);
                table.load('index/columns');
            loadTable(table, table.parent()[0].id.includes('Rewrite')?'pvr':'pv', 0, 128, tables[b].id, table.parent()[0].id.includes('Unfound'));
        }}), --i*3000)
}})
}}

function toRow(ver){{
    return `<tr style='background-color: #${{ver.Color}};${{(ver.Color == '93C47D' ? ' font-size: 0.7em;' : '')}}'>
    <td>${{ver.FoundStatus}}</td>
    <td>${{ver.Type}}</td>
    <td>${{ver.Id}}</td>
    <td>${{ver.Version}}</td>
    <td>${{ver.Pvn}}</td>
    <td>${{GetHyperLink(ver.ReleaseDate)}}</td>
    <td>${{GetHyperLink(ver.ProofOfExistence)}}</td>
    <td>${{ver.Format}}</td>
    <td>${{GetHyperLink(ver.Notes)}}</td>
    <td>${{ver.Md5}}</td>
    <td>${{ver.Sha256}}</td>
</tr>`;
}}
function GetHyperLink(x){{ return x+'';}}
</script>
<div class='tab'>
  <button class='tablinks' onclick='openTab(event, ""Rewrite"")'>Rewrite</button>
  <button class='tablinks active' onclick='openTab(event, ""RewriteUnfound"")'>Rewrite (unfound only)</button>
  <button class='tablinks' onclick='init()' style='float:right;'>Start loading</button>
</div>
<center>
  <div id='Rewrite' class='tabcontent'><div class=""loader"">Loading...</div></div>
  <div id='RewriteUnfound' class='tabcontent active'><div class=""loader"">Loading...</div></div>
</center>
<div id=footer>
</div>",
                ContentType = "text/html"
            };
        }

        private static string GetHyperLink(string markdown)
        {
            if (markdown == null) return "";
            if (!(markdown.Contains("http://") || markdown.Contains("https://"))) return markdown;
            if (!(markdown.Contains("[") && markdown.Contains(")") && markdown.Contains("(")
                  && markdown.Contains(")"))) return markdown;
            if (!markdown.StartsWith("[")) return markdown;
            string input = markdown.Replace("[", "");
            input = input.Replace("]", "");
            input = input.Replace(")", "");
            string[] input2 = input.Split('(');
            if (input2.Length == 1) return markdown;
            return $"<a href='{input2[1]}'>{input2[0]}</a>";
        }

        private static string GetIndexJson(Dictionary<string, List<McInfo>> index, bool includeFound, int start,
            int limit, string table)
        {
            Stopwatch sw = Stopwatch.StartNew();
            string a = "";
            if (!index.ContainsKey(table)) return "Table doesn't exist";
            List<McInfo> vers = index[table];
            return JsonConvert.SerializeObject(new
            {
                Total = vers.Count, Versions = vers.Skip(start * limit).Take(limit).Where(x =>
                {
                    if (includeFound) return true;
                    return x.Color == "93C47D";
                })
            }, Formatting.Indented);
            for (int i = start; i < Math.Min(vers.Count, start + limit); i++)
            {
                McInfo ver = vers[i];
                if (ver.Color == "93C47D" && !includeFound) continue;
                a +=
                    @$"<tr style='background-color: #{ver.Color};{(ver.Color == "93C47D" ? " font-size: 0.7em;" : "")}'>
    <td>{ver.FoundStatus}</td>
    <td>{ver.Type}</td>
    <td>{ver.Id}</td>
    <td>{ver.Version}</td>
    <td>{ver.Pvn}</td>
    <td>{GetHyperLink(ver.ReleaseDate)}</td>
    <td>{GetHyperLink(ver.ProofOfExistence)}</td>
    <td>{ver.Format}</td>
    <td>{GetHyperLink(ver.Notes)}</td>
    <td>{ver.Md5}</td>
    <td>{ver.Sha256}</td>
</tr>
";
            }

            return a;
        }

        private static string GetIndex(Dictionary<string, List<McInfo>> index, bool includeFound, string table)
        {
            Stopwatch sw = Stopwatch.StartNew();
            string a = "";
            if (!index.ContainsKey(table)) return "Table doesn't exist";
            List<McInfo> vers = index[table];
            foreach (var ver in vers)
            {
                if (ver.Color == "93C47D" && !includeFound) continue;
                a +=
                    @$"<tr style='background-color: #{ver.Color};{(ver.Color == "93C47D" ? " font-size: 0.7em;" : "")}'>
    <td>{ver.FoundStatus}</td>
    <td>{ver.Type}</td>
    <td>{ver.Id}</td>
    <td>{ver.Version}</td>
    <td>{ver.Pvn}</td>
    <td>{GetHyperLink(ver.ReleaseDate)}</td>
    <td>{GetHyperLink(ver.ProofOfExistence)}</td>
    <td>{ver.Format}</td>
    <td>{GetHyperLink(ver.Notes)}</td>
    <td>{ver.Md5}</td>
    <td>{ver.Sha256}</td>
</tr>
";
            }

            return a;
        }
    }

    public class IndexResponse
    {
        public IndexResponse(int total, List<McInfo> versions)
        {
            totalResults = total;
            results = versions;
        }

        public int totalResults = 0;
        public List<McInfo> results;
    }
}
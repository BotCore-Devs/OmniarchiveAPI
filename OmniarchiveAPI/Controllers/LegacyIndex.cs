﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using OmniarchiveIndexParser;
using Index = OmniarchiveIndexParser.Index;

namespace OmniarchiveAPI.Controllers
{
    //legacy code
    [Controller]
    [Route("/bbb")]
    public class LegacyIndexController : ControllerBase
    {
        // [HttpGet("legacyindex/pv")]
        // public ContentResult GetPv(bool includeFound = true)
        // {
        //     Dictionary<string, List<McInfo>> index = McUtil.ParseVersions(UseCache: true);
        //     return new ContentResult() {Content = GetIndex(index, includeFound), ContentType = "text/html"};
        // }

        [HttpGet("legacyindex/pvr")]
        public ContentResult GetPvr(bool includeFound = true)
        {
            Dictionary<string, List<McInfo>> index = Index.Instance.ParseVersionsRewrite(UseCache: true);
            return new ContentResult() {Content = GetIndex(index, includeFound), ContentType = "text/html"};
        }

        [HttpGet("legacyindex")]
        public ContentResult Get()
        {
            return new ContentResult
            {
                Content = $@"<script src=""https://code.jquery.com/jquery-3.5.1.min.js""></script>
<style>
  center{{ color: white; }}
  .avatar {{ border-radius:50%; height:10%; }}
  h1, h2, h3, h4, h5, h6, p {{font-family: 'Segoe UI';}}
  #footer {{
    position: fixed;
    bottom: 0;
    vertical-align: bottom;
    width: auto;
    height: 24px;
    border: none;
  }}
td {{
    white-space: nowrap;
    min-width: 0px;
}}
tr:hover, tr > *:hover, tr > * > *:hover {{
font-size: normal !important;
                }}
table {{min-width: 100%;}}


/* Style the tab */
.tab {{
  overflow: hidden;
  background-color: #262626;
}}

/* Style the buttons that are used to open the tab content */
.tab button {{
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  color: #ccc;
}}

/* Change background color of buttons on hover */
.tab button:hover {{
  background-color: #ddd;
  color: #420;
}}

/* Create an active/current tablink class */
.tab button.active {{
  background-color: #ccc;
  color: #420;
}}

/* Style the tab content */
.tabcontent {{
  display: none;
}}
.loader {{
  color: #ffffff;
  font-size: 90px;
  text-indent: -9999em;
  overflow: hidden;
  width: 1em;
  height: 1em;
  border-radius: 50%;
  margin: 72px auto;
  position: relative;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation: load6 1.7s infinite ease, round 1.7s infinite ease;
  animation: load6 1.7s infinite ease, round 1.7s infinite ease;
}}
@-webkit-keyframes load6 {{
  0% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
  5%,
  95% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
  10%,
  59% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.087em -0.825em 0 -0.42em, -0.173em -0.812em 0 -0.44em, -0.256em -0.789em 0 -0.46em, -0.297em -0.775em 0 -0.477em;
  }}
  20% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.338em -0.758em 0 -0.42em, -0.555em -0.617em 0 -0.44em, -0.671em -0.488em 0 -0.46em, -0.749em -0.34em 0 -0.477em;
  }}
  38% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.377em -0.74em 0 -0.42em, -0.645em -0.522em 0 -0.44em, -0.775em -0.297em 0 -0.46em, -0.82em -0.09em 0 -0.477em;
  }}
  100% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
}}
@keyframes load6 {{
  0% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
  5%,
  95% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
  10%,
  59% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.087em -0.825em 0 -0.42em, -0.173em -0.812em 0 -0.44em, -0.256em -0.789em 0 -0.46em, -0.297em -0.775em 0 -0.477em;
  }}
  20% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.338em -0.758em 0 -0.42em, -0.555em -0.617em 0 -0.44em, -0.671em -0.488em 0 -0.46em, -0.749em -0.34em 0 -0.477em;
  }}
  38% {{
    box-shadow: 0 -0.83em 0 -0.4em, -0.377em -0.74em 0 -0.42em, -0.645em -0.522em 0 -0.44em, -0.775em -0.297em 0 -0.46em, -0.82em -0.09em 0 -0.477em;
  }}
  100% {{
    box-shadow: 0 -0.83em 0 -0.4em, 0 -0.83em 0 -0.42em, 0 -0.83em 0 -0.44em, 0 -0.83em 0 -0.46em, 0 -0.83em 0 -0.477em;
  }}
}}
@-webkit-keyframes round {{
  0% {{
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }}
  100% {{
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }}
}}
@keyframes round {{
  0% {{
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }}
  100% {{
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }}
}}

</style>
<script>
function openTab(evt, cityName) {{
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class='tabcontent' and hide them
  tabcontent = document.getElementsByClassName('tabcontent');
  for (i = 0; i < tabcontent.length; i++) {{
    tabcontent[i].style.display = 'none';
  }}

  // Get all elements with class='tablinks' and remove the class 'active'
  tablinks = document.getElementsByClassName('tablinks');
  for (i = 0; i < tablinks.length; i++) {{
    tablinks[i].className = tablinks[i].className.replace(' active', '');
  }}

  // Show the current tab, and add an 'active' class to the button that opened the tab
  document.getElementById(cityName).style.display = 'block';
  evt.currentTarget.className += ' active';
}}
window.onload = function(e) {{
    //$('.tablinks').last().click();
  $('#RewriteUnfound').load('legacyindex/pvr?includeFound=false');
  $('#Rewrite').load('legacyindex/pvr');
  //$('#Original').load('legacyindex/pv')
                }};
</script>
<div class='tab'>
  <!--<button class='tablinks' onclick='openTab(event, ""Original"")'>Original</button>--->
  <button class='tablinks' onclick='openTab(event, ""Rewrite"")'>Rewrite</button>
  <button class='tablinks active' onclick='openTab(event, ""RewriteUnfound"")'>Rewrite (unfound only)</button>
</div>
<center>
  <div id='Original' class='tabcontent'><div class=""loader"">Loading...</div></div>
  <div id='Rewrite' class='tabcontent'><div class=""loader"">Loading...</div></div>
  <div id='RewriteUnfound' class='tabcontent' style='display: block;'><div class=""loader"">Loading...</div></div>
</center>
<div id=footer>
</div>",
                ContentType = "text/html"
            };
        }

        private static string GetHyperLink(string markdown)
        {
            if (markdown == null) return "";
            if (!(markdown.Contains("http://") || markdown.Contains("https://"))) return markdown;
            if (!(markdown.Contains("[") && markdown.Contains(")") && markdown.Contains("(")
                  && markdown.Contains(")"))) return markdown;
            if (!markdown.StartsWith("[")) return markdown;
            string input = markdown.Replace("[", "");
            input = input.Replace("]", "");
            input = input.Replace(")", "");
            string[] input2 = input.Split('(');
            if (input2.Length == 1) return markdown;
            return $"<a href='{input2[1]}'>{input2[0]}</a>";
        }

        private static string GetIndex(Dictionary<string, List<McInfo>> index, bool includeFound)
        {
            Stopwatch sw = Stopwatch.StartNew();
            string a = "";

            foreach (KeyValuePair<string, List<McInfo>> vers in index)
            {
                if (!vers.Key.Contains("java")) index.Remove(vers.Key);
            }

            foreach (KeyValuePair<string, List<McInfo>> vers in index)
            {
                a += $"<br>{vers.Key} ({vers.Value.Count} versions)<br><table>";
                a += @$"
<tr style='background-color: #000; color: #999;'>
    <td>Status</td>
    <td>Type</td>
    <td>ID</td>
    <td>Version</td>
    <td>PVN</td>
    <td>Release Date</td>
    <td>Proof of Existence</td>
    <td>Format</td>
    <td>Notes</td>
    <td>MD5</td>
    <td>SHA-256</td>
</tr>";
                foreach (McInfo ver in vers.Value)
                {
                    if (ver.Color == "93C47D" && !includeFound) continue;
                    a += @$"
<tr style='background-color: #{ver.Color};{(ver.Color == "93C47D" ? " font-size: 0.7em;" : "")}'>
    <td>{ver.FoundStatus}</td>
    <td>{ver.Type}</td>
    <td>{ver.Id}</td>
    <td>{ver.Version}</td>
    <td>{ver.Pvn}</td>
    <td>{GetHyperLink(ver.ReleaseDate)}</td>
    <td>{GetHyperLink(ver.ProofOfExistence)}</td>
    <td>{ver.Format}</td>
    <td>{GetHyperLink(ver.Notes)}</td>
    <td>{ver.Md5}</td>
    <td>{ver.Sha256}</td>
</tr>";
                }

                a += "</table>";
            }

            a = $"Generation time taken: {sw.ElapsedMilliseconds}ms<br>" + a;
            return a;
        }
    }
}
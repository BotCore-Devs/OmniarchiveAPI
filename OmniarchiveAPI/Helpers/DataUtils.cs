using System.Reflection;
using OmniarchiveIndexParser;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace OmniarchiveAPI.Helpers;

public class DataUtils
{
    public static string ListToHtmlTable<T>(List<T> list, string titlecolor = "#fff", string titlebg = "#000", string evenbg = "#333", string oddbg = "#444", string textcolor= "#000", int bordersize = 1)
    {
        string table = $"<table border={bordersize}>";
        var t = typeof(T);
        var props = t.GetProperties().Where(x=>x.GetMethod != null && !x.HasAttribute<TableIgnoreAttribute>());
        table += $"<tr style=\"color: {titlecolor}; background-color: {titlebg};\">";

        PropertyInfo? colorSrc = t.GetProperties().FirstOrDefault(x=>x.HasAttribute<RowColorAttribute>());
        foreach (var propertyInfo in props)
        {
            table += $"<td>{(propertyInfo.HasAttribute<FriendlyNameAttribute>() ? propertyInfo.GetCustomAttribute<FriendlyNameAttribute>().Name : propertyInfo.Name)}</td>";
        }
        table += "</tr>\n";
        bool odd = false;
        foreach (var item in list)
        {
            odd = !odd;
            var color = (odd ? oddbg : evenbg);
            if (colorSrc != null) color = (string) colorSrc.GetValue(item);
            table += $"<tr style=\"color: {textcolor}; background-color: {color};\">";
            foreach (var propertyInfo in props)
            {
                table += $"<td>{propertyInfo.GetValue(item)}</td>";
            }
            table += "</tr>\n";
        }
        table += "</table>";
        return table;
    }
}
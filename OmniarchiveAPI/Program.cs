using System.Diagnostics;
using System.Net;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.EntityFrameworkCore;
using OmniarchiveAPI;
using Sentry;
using Index = OmniarchiveIndexParser.Index;
using Timer = System.Timers.Timer;


//Index.Instance = new Index();
//Index.Instance.ParseVersionsRewrite();
var webCfg = Config.Read();
webCfg.Save();

var envname = webCfg.SentryEnvironment;
if (envname.Length < 1)
{
    Console.WriteLine("Environment name not set! Using hostname, to change this, set in Config.json!");
    envname = Environment.MachineName;
}

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHttpLogging(o => o.LoggingFields = HttpLoggingFields.All);
builder.Services.AddLogging(o => o.AddSystemdConsole());
builder.WebHost.UseSentry(o =>
{
    o.Dsn = "https://ded8147d51084bfc808c4d73b7b583b1@sentry.thearcanebrony.net/3";
    o.TracesSampleRate = 1.0;
    o.AttachStacktrace = true;
    o.MaxQueueItems = int.MaxValue;
    o.StackTraceMode = StackTraceMode.Original;
    o.Environment = envname;
});
/*var cfg = DbConfig.Read();
cfg.Save();
using (var db = new Db(new DbContextOptionsBuilder<Db>()
           .UseNpgsql(
               $"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port};Include Error Detail=true")
           .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options))
{
    db.Database.Migrate();
    db.SaveChanges();
    foreach (var dbBot in db.Bots) Console.WriteLine($"Listening on {dbBot.Name}.localhost -> {dbBot.FriendlyName}");
}

builder.Services.AddDbContextPool<Db>(optionsBuilder =>
{
    optionsBuilder
        .UseNpgsql(
            $"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port}")
        .LogTo(str => Debug.WriteLine(str), LogLevel.Information).EnableSensitiveDataLogging();
});*/
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseWebSockets();

app.UseRouting();
app.UseSentryTracing();
app.UseAuthorization();

app.UseEndpoints(endpoints => { endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}"); });
app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
app.Use((context, next) =>
{
    context.Response.Headers["Content-Type"] += "; charset=utf-8";
    return next.Invoke();
});
app.MapControllers();

SentrySdk.CaptureMessage("BotCore.Web started!");
app.Run();